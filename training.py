# Calcul de la température moyenne au pas d'une heure le 10-04-2006
#******************************************************************

#******************** Chargement de packages **********************
import pandas as pd
import yaml
import os
import io
import sys

#******************** Lecture du fichier de config.yaml ***********
params = yaml.safe_load(open("params.yaml"))['training']
data_path = sys.argv[1]+"/out.csv"

#******************** Chargement du dataset ***********************
df = pd.read_csv(data_path, parse_dates = ["Formatted Date"])
df = df.head(params['num_rows'])
print(df)

#************* Température moyenne à la date du 10-04-2006 ********
mean = round(df['Temperature (C)'].mean(), 2)
 
os.makedirs(os.path.join("output", "training"), exist_ok = True)
output_train = os.path.join("output", "training", "out.tsv")

print(f"La température moyenne est de", mean, "°C")
with io.open(output_train, "w", encoding="utf8") as fd_out_train:
    fd_out_train.write(str(mean))
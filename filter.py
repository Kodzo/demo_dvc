#*******************************************************************************
# Ce script permet de filtrer les températures au pas de 1h : 10-04-2006.      *
#*******************************************************************************

#**************************** Importation des packages ************************* 
import pandas as pd
import yaml
import os 
import io

#*************************** Fichier de configuration ***************************
params = yaml.safe_load(open("params.yaml"))['filter']
df = pd.read_csv("data/meteo2006.csv", parse_dates=["Formatted Date"])
df = df[df['Temperature (C)'] > params['value']]
df = df[['Formatted Date', 'Temperature (C)']]
print(df)

#*************************** Dossier de sauvegarde ******************************
os.makedirs(os.path.join("output", "filter"), exist_ok = True)
df.to_csv("output/filter/out.csv")
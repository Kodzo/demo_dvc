Démo montrant le versioning des données avec DVC, la création des pipelines et la reproductibilité 

des expériences avec DVC :

1- Créer des versions de données avec DVC

2- Créer des Pipelines avec DVC

3- Reproductibilité des expériences

4- Fichier dvc.yaml

5- Fichier dvc.lock

